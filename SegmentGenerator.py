# -*- coding: utf-8 -*-
"""
Created on Tue May 26 14:51:41 2020

Генератор сети трещин

@author: serge
"""

import numpy as np
import math
from math import pi, cos, sin
import matplotlib.pyplot as plt
from matplotlib.widgets import TextBox, Button
import matplotlib as mpl
from matplotlib.path import Path
import matplotlib.patches as patches
from sympy import Point, Polygon, Line, Segment, Circle #ЧИТАЙ ХЕЛП В SymPy Geometry Module http://docs.sympy.org/latest/modules/geometry/index.html 
#from sympy.plotting.pygletplot import PygletPlot as Plot
import random
import time

class Fracture:
    def __init__(self):
        #self.fig=None
        #self.ax=None
        self.fig = plt.figure(num="Генератор сети трещин",figsize=(16, 8)) #ЗАДАТЬ ИМЯ РИСУНКА И РАЗМЕРЫ ВСЕГО РИСУНКА В ДЮЙМАХ
        self.ax = self.fig.add_subplot() #ЗАДАТЬ ГРАФИК
        self.SS=None
        self.DomainXlb=None # X left bottom
        self.DomainYlb=None
        self.DomainXrt=None # X right top
        self.DomainYrt=None
        self.n_blocks_X=None
        self.n_blocks_Y=None
        self.nf=None
        self.p=None
        self.nextYcent=None
        self.prevYcent=None
        self.prevXcent=None
        self.nextLen=None
        #self.TempPoint=None
        
    def DrawSeg(self,seg):
        
        plt.sca(self.ax)
        
        #self.ax.plot(Point(0,0).x.evalf(),Point(0,0).y.evalf(), 'r*')
        self.verts=[seg.points[0],seg.points[1]]
        self.codes=[Path.MOVETO, Path.LINETO]
        self.path = Path(self.verts, self.codes)
        self.patch = patches.PathPatch(self.path, facecolor='none', lw=2)
        self.ax.add_patch(self.patch)
        plt.axis('auto')
        plt.show()
        
    def DrawPoint(self,p):
        
        plt.sca(self.ax)
        
        self.ax.plot(p.x,p.y,'r*')
        plt.axis('auto')
        plt.show()        
        
    
    def SegPoints(self,ang,length,centerX,centerY):
        
        
        x0=centerX-0.5*length*cos(ang*pi/180)
        y0=centerY-0.5*length*sin(ang*pi/180)
        x1=centerX+0.5*length*cos(ang*pi/180)
        y1=centerY+0.5*length*sin(ang*pi/180)
        return [Point(x0,y0),Point(x1,y1)]
    
    def RandPoint(self, p1,p2):
        xbwn=random.uniform(p1.x,p2.x)#x between p1 and p2
        ybwn=(p2.y-p1.y)/(p2.x-p1.x)*xbwn+p1.y-p1.x*(p2.y-p1.y)/(p2.x-p1.x)
        return Point(xbwn,ybwn)
    
    def point_inside_polygon(self,x,y,poly):

        n = len(poly)
        inside =False
    
        p1x,p1y = poly[0]
        for i in range(n+1):
            p2x,p2y = poly[i % n]
            if y > min(p1y,p2y):
                if y <= max(p1y,p2y):
                    if x <= max(p1x,p2x):
                        if p1y != p2y:
                            xinters = (y-p1y)*(p2x-p1x)/(p2y-p1y)+p1x
                        if p1x == p2x or x <= xinters:
                            inside = not inside
            p1x,p1y = p2x,p2y
    
        return inside
        
    def GenerateNaturalV1(self):
        
        Pol=[]
        
        Xpol=[
              self.DomainXlb,
              self.DomainXlb+(self.DomainXrt - self.DomainXlb)/3,
              self.DomainXlb+(self.DomainXrt - self.DomainXlb)*2/3,
              self.DomainXrt
              ]
        
        Ypol=[
              self.DomainYlb,
              self.DomainYlb+(self.DomainYrt-self.DomainYlb)/3,
              self.DomainYlb+(self.DomainYrt-self.DomainYlb)*2/3,
              self.DomainYrt
              ]
        
        p=[
            Point(Xpol[0],Ypol[0]), Point(Xpol[1],Ypol[0]), Point(Xpol[2],Ypol[0]), Point(Xpol[3],Ypol[0]),
            Point(Xpol[0],Ypol[1]), Point(Xpol[1],Ypol[1]), Point(Xpol[2],Ypol[1]), Point(Xpol[3],Ypol[1]),
            Point(Xpol[0],Ypol[2]), Point(Xpol[1],Ypol[2]), Point(Xpol[2],Ypol[2]), Point(Xpol[3],Ypol[2]),
            Point(Xpol[0],Ypol[3]), Point(Xpol[1],Ypol[3]), Point(Xpol[2],Ypol[3]), Point(Xpol[3],Ypol[3])
          ]
        

        #   12___13___14___15
        #   |  6 |  7 |  8 |
        #   8____9____10___11
        #   |  3 |  4 |  5 |
        #   4____5____6____7
        #   |  0 |  1 |  2 |
        #   0____1____2____3  
        
        #Создаем массив Полигонов
        
        Pol.append([p[0],p[1],p[5],p[4]])
        Pol.append([p[1],p[2],p[6],p[5]])
        Pol.append([p[2],p[3],p[7],p[6]])
        Pol.append([p[4],p[5],p[9],p[8]])
        Pol.append([p[5],p[6],p[10],p[9]])
        Pol.append([p[6],p[7],p[11],p[10]])
        Pol.append([p[8],p[9],p[13],p[12]])
        Pol.append([p[9],p[10],p[14],p[13]])
        Pol.append([p[10],p[11],p[15],p[14]])
        
        
        
        
        s0=[]
        s1=[]
        s2=[]
        s3=[]
        s4=[]
        s5=[]
        s6=[]
        s7=[]
        s8=[]
        i0=0
        i1=0
        i2=0
        i3=0
        i4=0
        i5=0
        i6=0
        i7=0
        i8=0
        for i in range(100):
            angle=0 #random.uniform(0,90)*pi/180
            length=random.uniform(1.5,3)
            centerx=random.uniform(0,50)
            centery=random.uniform(0,50)
            CentPtest=Point(centerx,centery)
            
            flag0=self.point_inside_polygon(centerx,centerx,Pol[0])
            flag1=self.point_inside_polygon(centerx,centerx,Pol[1])
            flag2=self.point_inside_polygon(centerx,centerx,Pol[2])
            flag3=self.point_inside_polygon(centerx,centerx,Pol[3])
            flag4=self.point_inside_polygon(centerx,centerx,Pol[4])
            flag5=self.point_inside_polygon(centerx,centerx,Pol[5])
            flag6=self.point_inside_polygon(centerx,centerx,Pol[6])
            flag7=self.point_inside_polygon(centerx,centerx,Pol[7])
            flag8=self.point_inside_polygon(centerx,centerx,Pol[8])
            if (flag0==1):
                distmin=0
                dist=[]
                if (i0>0)&(s0!=[]):
                    
                    while (distmin<=1.5):
                        for j in range(len(s0)):
                            dist.append(s0[j].distance(CentPtest))
                        distmin=min(dist)
                        if (distmin<=1.5):
                            centerx=random.uniform(0,50)
                            centery=random.uniform(0,50)
                            CentPtest=Point(centerx,centery)
                            dist=[]
                      
                SP=self.SegPoints(angle,length,centerx,centery)
                s0.append(Segment(SP[0],SP[1]))
                self.DrawSeg(s0[i0])
                i0=i0+1
                plt.pause(0.05)
            if (flag1==1):
                distmin=0
                dist=[]
                if (i1>0)&(s1!=[]):
                    
                    while (distmin<=1.5):
                        for j in range(len(s1)):
                            dist.append(s1[j].distance(CentPtest))
                        distmin=min(dist)
                        if (distmin<=1.5):
                            centerx=random.uniform(0,50)
                            centery=random.uniform(0,50)
                            CentPtest=Point(centerx,centery)
                            dist=[]
                      
                SP=self.SegPoints(angle,length,centerx,centery)
                s1.append(Segment(SP[0],SP[1]))
                self.DrawSeg(s1[i1])
                i1=i1+1
                plt.pause(0.05)
                
            if (flag2==1):
                distmin=0
                dist=[]
                if (i2>0)&(s2!=[]):
                    
                    while (distmin<=1.5):
                        for j in range(len(s2)):
                            dist.append(s2[j].distance(CentPtest))
                        distmin=min(dist)
                        if (distmin<=1.5):
                            centerx=random.uniform(0,50)
                            centery=random.uniform(0,50)
                            CentPtest=Point(centerx,centery)
                            dist=[]
                      
                SP=self.SegPoints(angle,length,centerx,centery)
                s2.append(Segment(SP[0],SP[1]))
                self.DrawSeg(s2[i2])
                i2=i2+1
                plt.pause(0.05)
            if (flag3==1):
                distmin=0
                dist=[]
                if (i3>0)&(s3!=[]):
                    
                    while (distmin<=1.5):
                        for j in range(len(s3)):
                            dist.append(s3[j].distance(CentPtest))
                        distmin=min(dist)
                        if (distmin<=1.5):
                            centerx=random.uniform(0,50)
                            centery=random.uniform(0,50)
                            CentPtest=Point(centerx,centery)
                            dist=[]
                      
                SP=self.SegPoints(angle,length,centerx,centery)
                s3.append(Segment(SP[0],SP[1]))
                self.DrawSeg(s3[i3])
                i3=i3+1
                plt.pause(0.05)
            if (flag4==1):
                distmin=0
                dist=[]
                if (i4>0)&(s4!=[]):
                    
                    while (distmin<=1.5):
                        for j in range(len(s4)):
                            dist.append(s4[j].distance(CentPtest))
                        distmin=min(dist)
                        if (distmin<=1.5):
                            centerx=random.uniform(0,50)
                            centery=random.uniform(0,50)
                            CentPtest=Point(centerx,centery)
                            dist=[]
                      
                SP=self.SegPoints(angle,length,centerx,centery)
                s4.append(Segment(SP[0],SP[1]))
                self.DrawSeg(s4[i4])
                i4=i4+1
                plt.pause(0.05)
                
            if (flag5==1):
                distmin=0
                dist=[]
                if (i5>0)&(s5!=[]):
                    
                    while (distmin<=1.5):
                        for j in range(len(s5)):
                            dist.append(s5[j].distance(CentPtest))
                        distmin=min(dist)
                        if (distmin<=1.5):
                            centerx=random.uniform(0,50)
                            centery=random.uniform(0,50)
                            CentPtest=Point(centerx,centery)
                            dist=[]
                      
                SP=self.SegPoints(angle,length,centerx,centery)
                s5.append(Segment(SP[0],SP[1]))
                self.DrawSeg(s5[i5])
                i5=i5+1
                plt.pause(0.05)
            if (flag6==1):
                distmin=0
                dist=[]
                if (i6>0)&(s6!=[]):
                    
                    while (distmin<=1.5):
                        for j in range(len(s6)):
                            dist.append(s6[j].distance(CentPtest))
                        distmin=min(dist)
                        if (distmin<=1.5):
                            centerx=random.uniform(0,50)
                            centery=random.uniform(0,50)
                            CentPtest=Point(centerx,centery)
                            dist=[]
                      
                SP=self.SegPoints(angle,length,centerx,centery)
                s6.append(Segment(SP[0],SP[1]))
                self.DrawSeg(s6[i6])
                i6=i6+1
                plt.pause(0.05)
            if (flag7==1):
                distmin=0
                dist=[]
                if (i7>0)&(s7!=[]):
                    
                    while (distmin<=1.5):
                        for j in range(len(s7)):
                            dist.append(s7[j].distance(CentPtest))
                        distmin=min(dist)
                        if (distmin<=1.5):
                            centerx=random.uniform(0,50)
                            centery=random.uniform(0,50)
                            CentPtest=Point(centerx,centery)
                            dist=[]
                      
                SP=self.SegPoints(angle,length,centerx,centery)
                s7.append(Segment(SP[0],SP[1]))
                self.DrawSeg(s7[i7])
                i7=i7+1
                plt.pause(0.05)
            if (flag8==1):
                distmin=0
                dist=[]
                if (i8>0)&(s8!=[]):
                    
                    while (distmin<=1.5):
                        for j in range(len(s8)):
                            dist.append(s8[j].distance(CentPtest))
                        distmin=min(dist)
                        if (distmin<=1.5):
                            centerx=random.uniform(0,50)
                            centery=random.uniform(0,50)
                            CentPtest=Point(centerx,centery)
                            dist=[]
                      
                SP=self.SegPoints(angle,length,centerx,centery)
                s8.append(Segment(SP[0],SP[1]))
                self.DrawSeg(s8[i8])
                i8=i8+1
                plt.pause(0.05)
            
            
        
    def GenerateNaturalV2(self):
        
        s=[]
        for i in range(100):
            angle=0 #random.uniform(0,90)*pi/180
            length=random.uniform(1.5,3)
            centerx=random.uniform(0,50)
            centery=random.uniform(0,50)
            CentPtest=Point(centerx,centery)
        
            distmin=0
            dist=[]
            if (i>0):
                
                while (distmin<=1.5):
                    for j in range(len(s)):
                        dist.append(s[j].distance(CentPtest))
                    distmin=min(dist)
                    if (distmin<=1.5):
                        centerx=random.uniform(0,50)
                        centery=random.uniform(0,50)
                        CentPtest=Point(centerx,centery)
                        dist=[]
                  
            SP=self.SegPoints(angle,length,centerx,centery)
            s.append(Segment(SP[0],SP[1]))
            self.DrawSeg(s[i])
            plt.pause(0.05)
            
    def STR0(self,m,J):
 
        if ((m%J)!=0):
            aux=math.floor(m/J)+1
        else:
            aux=math.floor(m/J)
        
        return aux
 
 
    def COLUMN0(self,m,J):
        if ((m%J)!=0):
            aux=(m%J)
        else:
            aux=J
        
        return aux
	 
    def SeqFracBlockGen(self):
        """
        Данная функция позволяет замостить область блоками прямоугольной формы,
        при этом каждый блок содержит небольшое количество трещин.
        В отличие от функции BlockNatFracGen() здесь происходит генерирование
        не только естественных, но и потенциальных трещин (трещин перемычек). Причем генерирование
        трещин внутри каждого блока происходит поочередно: естетсвенная, потенциальная,
        естественная, потенциальная и т.д. Для связывания в единую сеть требуется
        только соединить перемычками примыкающие блоки. Такой принцип позволяет
        управлять характеристиками сети трещин (например, плотностью) для адаптации на данные
        МСМ, а также для моделирования DRV.

        Returns
        -------
        None.

        """
# =============================================================================
#         self.n_blocks_X=3 #число блоков вдоль расчетной области по X
#         self.n_blocks_Y=4 #число блоков вдоль расчетной области по Y
# =============================================================================
        BlockSizeX=(self.DomainXrt-self.DomainXlb)/self.n_blocks_X #размер стороны блока по X
        BlockSizeY=(self.DomainYrt-self.DomainYlb)/self.n_blocks_Y #размер стороны блока по Y
        print("BlockSizeY = ", BlockSizeY)
        px=[] #координаты Х отсекающие блоки друг от друга
        py=[]
        for j in range(self.n_blocks_X+1):
            px.append(self.DomainXlb+BlockSizeX*j)
            
        for j in range(self.n_blocks_Y+1):
            py.append(self.DomainYlb+BlockSizeY*j)
        
        
        
        
        """
        Может быть несколько сценариев генерирования типовых блоков с сетью трещин:
            1) В каждом блоке проходит одна магистральная трещина (от скважины)
            вдоль максимальных горизонтальных напряжений, которая пересекает
            все естественные трещины на своем пути и "подключает" их к фильтрации
            2) Искусственная трещина от скважины доходит до ближайшей естественной трещины где останавливается,
            дальнейший рост уже происходит из какого-то зародыша на стенке "подключенной" естественной
            трещины в направлении максимальных напряжений. Предполагается, что зародыш
            появляется только на одной из стенок естественной трещины, а не пересекает сразу обе
            стенки естественной трещины. Новая трещина из зародыша растет пока не встретит
            другую естественную трещину. В данном сценарии предполагается, что все естественные трещины
            ориентированы вдоль минимальных горизонтальных напряжений. Такое возможно при небольшом
            контрасте напряжений. В жизни естественные трещины могут быть ориентированы различным
            образом, однако статистическое количество приобщенных естестественных трещин не зависит
            от выбранной модели естественных трещин (гипотеза). Кроме того, предполагается,
            что трещины растут только в направлении "от скважины в пласт", что, в некоторых случаях,
            связано с влиянием сжимающих напряжении, наведенных открытой частью растущей трещины
            (однако в общем случае это влияние не всегда заметно).
        
        Для упрощения процесса генерация сети трещин используются следующие ограничения:
            1. Горизонтальные естественные трещины всегда последовательно соединяются
            вертикальнми перемычками, т.е. не может быть перемычки, соединяющей
            две горизонтальные трещины, которые не являются непосредственными соседями
            2. Для связи двух соседних блоков используются горизонтальные и вертикальные перемычки,
            которые генерируются после того, как все блоки заполнены трещинами.
            3. При последовательной генерации (внутри блока) естественных горизонтальных трещин и 
            потенциальных трещин (т.е. вертикальных перемычек), принимается, что перемычка 
            начинается в центре сгенерированной горизонтальной естественной трещины
            и заканчивается где-то в случайном месте следующей горизонтальной естественной трещины.
            4. Условие 3 накладывает ограничение на параметры следующей горизонтальной
            трещины. В частности, центр следующей горизонтально трещины и ее длина должны быть выбраны так, 
            чтобы вертикальная трещина-перемычка (она же потенциальная трещина) могла соединить (подключить)
            предыдущую и текущую горизонтальную трещины.
            5. Из предложенной схемы следует, что горизонтальные трещины имеют
            четные номера (начиная с нуля), а вертикальные трещины перемычки имеют нечетные номера
        
        """
        
        # Ниже пример блока с трещинами, удовлетворяющими ограничениям.
        # 
        #       _______           8
        #           |             7
        #       ____|____         6
        #             |           5
        #       ______|______     4
        #        |                3
        #   _____|_____           2
        #           |             1
        #        ___|___          0
        
        s0=[[] for i in range(5)] #список, который содержит сегменты каждого из 5 блоков
        p0=[[] for i in range(5)] #список, который содержит случайные точки внутри сегментов каждого из 5 блоков
        nhor=self.nf//2 #число горизонтальных слоев (при этом self.nf%2!=0 )
        #nvert=self.nf//2
        stepy=(py[1]-py[0])/nhor
        print("stepy = ", stepy)
        block_width=px[1]-px[0]
        for k in range(5): #число разновидностей блоков для того, чтобы замостить область
            i0=0
            while (i0<self.nf):#число горизонтальных трещин в каждом блоке
                
                if(i0==0): #первая горизонтальная трещина
                    angle=0 #random.uniform(0,90)*pi/180
                    length=random.uniform(0.1*block_width,0.5*block_width)
                    centerx=self.DomainXlb+0.5*(px[1]-px[0])#random.uniform(px[0],px[1])
                    centery=self.DomainYlb+i0*stepy#random.uniform(0.48*stepy,0.5*stepy)#сама линия трещины расположена в определенном интервале
                    #CentPtest=Point(centerx,centery)
                    self.prevYcent=centery
                    self.prevXcent=centerx
                    #distmin=0
                    #dist=[]
                
                
                if (i0>0)&(s0[k]!=[])&(i0%2!=0): #нечетная вертикальная потенциальная трещина
                    angle=90
                    aux=(i0+1)//2
                    self.nextYcent=self.DomainYlb+aux*stepy#random.uniform((i0)*stepy+0.48*stepy,(i0)*stepy+0.5*stepy)#Y координата следующей горизонтальной трещины
                    length=self.nextYcent-self.prevYcent#длина вертикальной потенциальной трещины-перемычки
                    centery=0.5*(self.nextYcent+self.prevYcent)#посредине между двумя соседними горизонтальными трещинами
                    self.nextLen=random.uniform(0.1*block_width,0.3*block_width)#длина следующей горизонтальной трещины
                    hl=0.5*self.nextLen# half length
                    #lindent=0.1*block_width+0.5*length#отступ слева от левой границы блока
                    #rindent=0.9*block_width-0.5*length#отступ справа от правой границы блока
                    centerx=self.prevXcent
                    
                    
                    #CentPtest=Point(centerx,centery)
# =============================================================================
#                     distmin=0
#                     dist=[]
#                     while (distmin<=1.5):
#                         for j in range(len(s0[k])):
#                             dist.append(s0[k][j].distance(CentPtest))
#                         distmin=min(dist)
#                         if (distmin<=3):
#                             centerx=random.uniform(px[0],px[1])
#                             centery=random.uniform(py[0],py[1])
#                             CentPtest=Point(centerx,centery)
#                             dist=[]
# =============================================================================
                if (i0>0)&(s0[k]!=[])&(i0%2==0):
                    angle=0
                    length=self.nextLen
                    centery=self.nextYcent
                    hl=0.5*self.nextLen# half length
                    centerx=random.uniform(self.prevXcent-hl,self.prevXcent+hl)
                    self.prevYcent=centery
                    self.prevXcent=centerx
                
                
                SP=self.SegPoints(angle,length,centerx,centery)
                #RP=self.RandPoint(SP[0],SP[1])
                s0[k].append(Segment(SP[0],SP[1]))
                #p0[k].append(RP)
                i0=i0+1
        
        
        #print("s0[0] = ",s0[0])
        
        N=self.n_blocks_X*self.n_blocks_Y #количество блоков для покрытия области
        
        #Переписываем значения координат точек сегментов из s0[i] в обычные переменные
        #поскольку прямое суммирование к s0[i][k].points[0].x генерирует ошибку
        
        x0=[[] for i in range(5)] #список 0-координат x сегментов из s0[i]
        y0=[[] for i in range(5)] #список 0-координат y сегментов из s0[i]
        x1=[[] for i in range(5)] #список 1-координат x сегментов из s0[i]
        y1=[[] for i in range(5)] #список 1-координат y сегментов из s0[i]
        
        #точки на сегментах из которых будут расти трещины ГРП
# =============================================================================
#         p0x=[[None]*self.nf for i in range(5)]
#         p0y=[[None]*self.nf for i in range(5)]
# =============================================================================
        
        for k in range(5):
            for i in range(self.nf):
                x0[k].append(s0[k][i].points[0].x.evalf())
                y0[k].append(s0[k][i].points[0].y.evalf())
                x1[k].append(s0[k][i].points[1].x.evalf())
                y1[k].append(s0[k][i].points[1].y.evalf())
                #print("k = ",k,", i = ",i)
# =============================================================================
#                 p0x[k][i]=(p0[k][i].x.evalf())
#                 p0y[k][i]=(p0[k][i].y.evalf())
# =============================================================================
        #значения координат точек сегментов переписаны
                
        self.p=[[None]*self.nf for i in range(N)]
        #self.py=[[None]*self.nf for i in range(N)]
        
        s=[[None]*self.nf for i in range(N)]

        
        for i in range(N):
            B=random.randint(0, 4)


            
            nx=self.n_blocks_X
            
            Istr=self.STR0(i+1,nx)-1
            Jcol=self.COLUMN0(i+1,nx)-1
            
            #print('i = ', i, ' Istr = ', Istr, ' Jcol = ', Jcol,' BlockSizeY*Istr = ', BlockSizeY*Istr,' BlockSizeX*Jcol = ', BlockSizeX*Jcol, )
            
            for k in range(self.nf):
                s[i][k]=(Segment(Point(x0[B][k]+BlockSizeX*Jcol,y0[B][k]+BlockSizeY*Istr),
                                Point(x1[B][k]+BlockSizeX*Jcol,y1[B][k]+BlockSizeY*Istr)))
                
                #self.p[i][k]=Point(p0x[B][k]+BlockSizeX*Jcol,p0y[B][k]+BlockSizeY*Istr)
                
                #print("Seg(",k,")= [P1(",s0[B][k].points[0].x.evalf(),",",s0[B][k].points[0].y.evalf(),"), P2(",s0[B][k].points[1].x.evalf(),",",s0[B][k].points[1].y.evalf(),")]")

                self.DrawSeg(s[i][k])
                
                #self.DrawPoint(self.p[i][k])
                
                plt.pause(0.05)       
    
    def BlockNatFracGen(self):
        """
        Данная функция позволяет замостить область блоками прямоугольной формы
        по аналогии с укладкой тротуарной плитки.
        Каждый блок содержит небольшое количество трещин, удовлетворяющих
        некоторым (например, статистическим) критериям и ограничениям. В частности,
        у каждой естественной трещины есть длина, координата центра, угол поворота.
        Кроме того, трещины не могут распологаться слишком близко друг к другу
        из-за сложностей последующей генерации сетки КЭ и численного расчета.
        Поскольку проверка ограничений для каждой вновь добавляемой трещины требует
        проверки ограничений на близость с каждой из уже добавленных трещин, то
        трещины генерируются небольшими блоками по пять трещин в каждом блоке.
        Всего генерируется 5 разновидностей блоков, а далее производится
        замощение расчетной области типовыми блоками. Причем отдельный блок 
        выбирается случайно из 5 вариантов и помещается в свободную 
        (незамощенную) часть расчетной области, при этом возникает эффект 
        псевдослучайной генерации трещин.
        

        Returns
        -------
        None.

        """
        self.n_blocks_X=3 #число блоков вдоль расчетной области по X
        self.n_blocks_Y=3 #число блоков вдоль расчетной области по Y
        BlockSizeX=(self.DomainXrt-self.DomainXlb)/self.n_blocks_X #размер стороны блока по X
        BlockSizeY=(self.DomainYrt-self.DomainYlb)/self.n_blocks_Y #размер стороны блока по Y
        px=[] #координаты Х отсекающие блоки друг от друга
        py=[]
        for j in range(self.n_blocks_X+1):
            px.append(self.DomainXlb+BlockSizeX*j)
            
        for j in range(self.n_blocks_Y+1):
            py.append(self.DomainYlb+BlockSizeY*j)
        
        
        s0=[[] for i in range(5)] #список, который содержит сегменты каждого из 5 блоков
        p0=[[] for i in range(5)] #список, который содержит случайные точки внутри сегментов каждого из 5 блоков
        for k in range(5): #число разновидностей блоков для того, чтобы замостить область
            i0=0
            while (i0<self.nf):#число трещин в каждом блоке
                angle=0 #random.uniform(0,90)*pi/180
                length=random.uniform(1.5,3)
                centerx=random.uniform(px[0],px[1])
                centery=random.uniform(py[0],py[1])
                CentPtest=Point(centerx,centery)
                distmin=0
                dist=[]
                
                
                if (i0>0)&(s0[k]!=[]):
                    while (distmin<=1.5):
                        for j in range(len(s0[k])):
                            dist.append(s0[k][j].distance(CentPtest))
                        distmin=min(dist)
                        if (distmin<=3):
                            centerx=random.uniform(px[0],px[1])
                            centery=random.uniform(py[0],py[1])
                            CentPtest=Point(centerx,centery)
                            dist=[]
                          
                SP=self.SegPoints(angle,length,centerx,centery)
                RP=self.RandPoint(SP[0],SP[1])
                s0[k].append(Segment(SP[0],SP[1]))
                p0[k].append(RP)
                i0=i0+1
                
        
        N=self.n_blocks_X*self.n_blocks_Y #количество блоков для покрытия области
        
        #Переписываем значения координат точек сегментов из s0[i] в обычные переменные
        #поскольку прямое суммирование к s0[i][k].points[0].x генерирует ошибку
        
        x0=[[] for i in range(5)] #список 0-координат x сегментов из s0[i]
        y0=[[] for i in range(5)] #список 0-координат y сегментов из s0[i]
        x1=[[] for i in range(5)] #список 1-координат x сегментов из s0[i]
        y1=[[] for i in range(5)] #список 1-координат y сегментов из s0[i]
        
        #точки на сегментах из которых будут расти трещины ГРП
        p0x=[[None]*self.nf for i in range(5)]
        p0y=[[None]*self.nf for i in range(5)]
        
        for k in range(5):
            for i in range(self.nf):
                x0[k].append(s0[k][i].points[0].x.evalf())
                y0[k].append(s0[k][i].points[0].y.evalf())
                x1[k].append(s0[k][i].points[1].x.evalf())
                y1[k].append(s0[k][i].points[1].y.evalf())
                
                p0x[k][i]=(p0[k][i].x.evalf())
                p0y[k][i]=(p0[k][i].y.evalf())
        #значения координат точек сегментов переписаны
                
        self.p=[[None]*self.nf for i in range(N)]
        #self.py=[[None]*self.nf for i in range(N)]
        
        s=[[None]*self.nf for i in range(N)]

        
        for i in range(N):
            B=random.randint(0, 4)


            
            nx=self.n_blocks_X
            
            Istr=self.STR0(i+1,nx)-1
            Jcol=self.COLUMN0(i+1,nx)-1
            
            print('i = ', i, ' Istr = ', Istr, ' Jcol = ', Jcol,' BlockSizeY*Istr = ', BlockSizeY*Istr,' BlockSizeX*Jcol = ', BlockSizeX*Jcol, )
            
            for k in range(self.nf):
                s[i][k]=(Segment(Point(x0[B][k]+BlockSizeX*Jcol,y0[B][k]+BlockSizeY*Istr),
                                Point(x1[B][k]+BlockSizeX*Jcol,y1[B][k]+BlockSizeY*Istr)))
                
                self.p[i][k]=Point(p0x[B][k]+BlockSizeX*Jcol,p0y[B][k]+BlockSizeY*Istr)
                
                #print("Seg(",k,")= [P1(",s0[B][k].points[0].x.evalf(),",",s0[B][k].points[0].y.evalf(),"), P2(",s0[B][k].points[1].x.evalf(),",",s0[B][k].points[1].y.evalf(),")]")

                self.DrawSeg(s[i][k])
                
                self.DrawPoint(self.p[i][k])
                
                plt.pause(0.05)
           
                
        
NatFrac=Fracture()

# =============================================================================
# NatFrac.DomainXlb=0
# NatFrac.DomainYlb=0
# NatFrac.DomainXrt=60
# NatFrac.DomainYrt=60
# NatFrac.nf=5
# =============================================================================

NatFrac.DomainXlb=-30
NatFrac.DomainYlb=-30
NatFrac.DomainXrt=30
NatFrac.DomainYrt=30
NatFrac.n_blocks_X=5 #число блоков вдоль расчетной области по X (число стадий)
NatFrac.n_blocks_Y=10 #число блоков вдоль расчетной области по Y
NatFrac.nf=3 #число взаимоперпендиклярных трещин в каждом блоке

start_time = time.time()

NatFrac.SeqFracBlockGen()

#NatFrac.BlockNatFracGen()

#NatFrac.GenerateNaturalV1()
end_time = time.time()
            
elapsed_time = end_time-start_time
print("elapsed_time = ",elapsed_time)


# =============================================================================
# start_time = time.time()
# NatFrac.GenerateNaturalV2()
# end_time = time.time()
#             
# elapsed_time2 = end_time-start_time
# print("elapsed_time2 = ",elapsed_time2)
# =============================================================================



plt.axis('auto')
plt.show()

